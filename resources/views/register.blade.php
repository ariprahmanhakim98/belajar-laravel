<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<form action="/welcome" method="POST">
		@csrf
		<h1>Buat Account Baru!</h1>
		<h3>Sign Up Form</h3>

		<label for="first">First name:</label><br><br>
		<input type="text" id="first" name="nama"><br><br>

		<label for="last">Last name:</label><br><br>
		<input type="text" id="last" name="nama2"><br><br>

		<label>Gender:</label><br><br>
		<input type="radio" name="jk">Male<br>
		<input type="radio" name="jk">Female<br>
		<input type="radio" name="jk">Other<br><br>

		<label>Nationality:</label><br><br>
		<select>
			<option>Indonesian</option>
			<option>Singaporean</option>
			<option>Malaysian</option>
			<option>Australian</option>
		</select><br><br>

		<label>Language Spoken:</label><br><br>
		<input type="checkbox" name="language">Bahasa Indonesia<br>
		<input type="checkbox" name="language">English<br>
		<input type="checkbox" name="language">Other<br><br>

		<label>Bio:</label><br><br>
		<textarea cols="40" rows="9"></textarea><br>

		<input type="submit" value="kirim">

	</form>
</body>
</html>